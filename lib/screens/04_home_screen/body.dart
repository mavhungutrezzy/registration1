
// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class Body extends StatelessWidget {

  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Center(
        child: Column(
          children: [
            SizedBox(height: 20.0.h),
            FloatingActionButton(
              onPressed: () => Navigator.pushNamed(context, '/screenA'),
              child: Text('A')
            ),
            SizedBox(height: 20.0.h),
            FloatingActionButton(
              onPressed: () => Navigator.pushNamed(context, '/screenb'),
              child: Text('B')
            ),
            SizedBox(height: 20.0.h),
            FloatingActionButton(
              onPressed: () => Navigator.pushNamed(context, '/profile'),
              child: Text('Profile')
            ),
          ]
        ),
      )
    );
  }
}
