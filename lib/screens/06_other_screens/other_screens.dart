// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import '../../components/barrel.dart';



class  ScreenA extends StatelessWidget {

  static String routeName = "/screenA";

  const ScreenA({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Color(0xff223263),
        elevation: 0,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back
          )
        ),
        title: MainText(text: 'Screen A',),
        centerTitle: true,
      ),
    
      body: Center(child: Text('Screen A')),
    );
  }
}




class  ScreenB extends StatelessWidget {

  static String routeName = "/screenB";

  const ScreenB({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Color(0xff223263),
        elevation: 0,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back
          )
        ),
        title: MainText(text: 'Screen B',),
        centerTitle: true,
      ),
    
      body: Center(child: Text('Screen B')),
    );
  }
}