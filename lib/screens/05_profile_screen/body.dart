
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/barrel.dart';


class Body extends StatelessWidget {

  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Column(
        children: [
          SizedBox(height: 20.0.h,),
          Center(
            child: Container(
              child: Stack(
                children: [
                  CircleAvatar(
                    radius: 60.0,
                    child: ClipRRect(
                        child: Image.network('https://bit.ly/3xhu3KF'),
                        borderRadius: BorderRadius.circular(70.0),
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    right: 0.0,
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blue.shade200,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.edit,
                          color: Colors.blue
                        ),
                      ),
                    )
                  )
                ],
              )
            )
          ),

          SizedBox(height: 50.0.h,),

          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0.w),
                child: Column(
                  children: [
                    TextFormField(
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person_outline),
                        hintText: 'Adivhaho Mavhungu',
                        hintStyle: GoogleFonts.poppins(
                          fontWeight: FontWeight.normal,
                          fontSize: 13.0.sp,
                          color: Color(0xff9098b1),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xffEBF0FF),
                          )
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(),
                          borderRadius: BorderRadius.circular(5)
                        )
                      ),
                      style: TextStyle(
                        color: Color(0xFF9098B1)
                      ),
                    ),
                    SizedBox(height: 30.0.h,),
            
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.email_outlined),
                        hintText: 'mavhungutrezzy@gmail.com',
                        hintStyle: GoogleFonts.poppins(
                          fontWeight: FontWeight.normal,
                          fontSize: 13.0.sp,
                          color: Color(0xff9098b1),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xffEBF0FF),
                          )
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(),
                          borderRadius: BorderRadius.circular(5)
                        )
                      ),
                      style: TextStyle(
                        color: Color(0xFF9098B1)
                      ),
                    ),
                    SizedBox(height: 30.0.h,),
                    TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.phone_outlined),
                        hintText: '+27 724 943 834',
                        hintStyle: GoogleFonts.poppins(
                          fontWeight: FontWeight.normal,
                          fontSize: 13.0.sp,
                          color: Color(0xff9098b1),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xffEBF0FF),
                          )
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(),
                          borderRadius: BorderRadius.circular(5)
                        )
                      ),
                      style: TextStyle(
                        color: Color(0xFF9098B1)
                      ),
                    ),
            
                    SizedBox(height: 30.0.h,),
            
                    Password(hintText: '* * * * * * * * * * *'),
                    SizedBox(height: 30.0.h,),
                    SigninSignup(text: 'Save Changes')
            
                  ],
                )
              ),
            ),
          )

          
        ]
      )
    );
  }
}
