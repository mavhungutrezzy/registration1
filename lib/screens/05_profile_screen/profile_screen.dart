// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import '../../components/barrel.dart';
import 'body.dart';



class  ProfileScreen extends StatelessWidget {

  static String routeName = "/profile";

  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Color(0xff223263),
        elevation: 0,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back
          )
        ),
        title: MainText(text: 'Edit Profile',),
        centerTitle: true,
      ),
    
      body: Body(),
    );
  }
}