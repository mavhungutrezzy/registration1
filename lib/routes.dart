
// ignore_for_file: prefer_const_constructors


import 'package:bulgari/screens/06_other_screens/other_screens.dart';
import 'package:flutter/material.dart';

import 'screens/01_splash_screen/splash_screen.dart';
import 'screens/02_login_screen/login_screen.dart';
import 'screens/03_signup_screen/signup_screen.dart';
import 'screens/04_home_screen/home_screen.dart';
import 'screens/05_profile_screen/profile_screen.dart';


final Map<String, WidgetBuilder> routes = {

  SplashScreen.routeName: (context) => SplashScreen(),
  LoginScreen.routeName: (context) => LoginScreen(),
  SignupScreen.routeName: (context) => SignupScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  ScreenA.routeName: (context) => ScreenA(),
  ScreenB.routeName: (context) => ScreenB(),
  
};